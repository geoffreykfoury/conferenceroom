


$(document).ready(function () {

    // page is now ready, initialize the calendar...

    var urls = new Array();
    var fullTimeArray = new Array();
    var globalRoomChecked;
    var list;
    var alreadyCalledUsers;

    fullTimeArray = ['06:00:00', '24:00:00'];


    urls[0] = "/large";
    urls[1] = "/window";
    urls[2] = "/small";


    var newSource = new Array();

    function users() {
        $.ajax({
            type: 'GET',
            url: contextRoot + '/getUsers',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {

            $.each(data, function (index, User) {

                $('.users').append("<option value='" + User.email + "'>" + User.firstName + " " + User.lastName + " --- " + User.email + "</option>");
            });

            alreadyCalledUsers = true;
        });
    }


    $('#date').datepicker({
        'format': 'yyyy-mm-dd',
        'autoclose': true,
        'daysOfWeekDisabled': [0, 6]
    });

    $('.st').timepicker({
        'scrollDefault': 'now',
        'minTime': '6:00am',
        'maxTime': '12:00am',
        'showDuration': false,
        'timeFormat': 'g:ia'
                //g:ia
    });

    $('.et').timepicker({
        'scrollDefault': 'now',
        'minTime': '6:00am',
        'maxTime': '12:00am',
        'showDuration': true,
        'timeFormat': 'g:ia'
    });


    $('.btn-radio').click(function (e) {
        var selectedDate = $('#date').val();
        dateTimes = new Array();


        globalRoomChecked = $(this).attr('id');
        if (!alreadyCalledUsers) {
            users();
        }

        $.ajax({
            type: 'GET',
            url: contextRoot + "/date/" + selectedDate + "/" + globalRoomChecked,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }, success: function (response) {

                meetingOverlap = new Array();

                if ($.trim(response)) {

                    $.each(response, function (index, bookedTimes) {

                        var timeArray = new Array();

                        var st = bookedTimes.startTime;
                        var et = bookedTimes.endTime;
                        timeArray.push(st);
                        timeArray.push(et);

                        meetingOverlap.push(et);
                        dateTimes.push(timeArray);
                    });

                } else {
                    //THIS IS AN EXAMPLE, ONLY WORKS FOR LATEST
                    dateTimes.pop();
                }

                $('.st').timepicker('option', 'disableTimeRanges', dateTimes);
                $('.et').timepicker('option', 'disableTimeRanges', dateTimes);

                $('.st').val("");
                $('.et').val("");
            }
        });
    });

//REFACTOR THIS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    $('.st').change(function () {

        $('.et').val("");
        var dateObjStart = $('.st').timepicker('getTime');
        var minEndTime = dateFormatter(dateObjStart);

        dateObjStart.setMinutes(dateObjStart.getMinutes() + 30);

        var minEndTimePlus30 = dateFormatter(dateObjStart);

        var regExp = new RegExp("(:|am|pm)", 'gi');
        var minEndTimeStripped = minEndTime.replace(regExp, '');
        minEndTimeStripped = minEndTimeStripped - 0;

        var beforeStartTime = new Array();
        beforeStartTime.push('06:00:00');
        beforeStartTime.push(minEndTimePlus30);

        dateTimes.push(beforeStartTime);

        for (var i = 0; i < meetingOverlap.length; i++) {

            //GET ALL THE INDIVIDUAL ENDTIME AND STRIP EXTRAS
            var regExps = new RegExp("(:|am|pm)", 'gi');
            var time = meetingOverlap[i];
            time = time.replace(regExp, '');

            //COMPARE STARTTIME MIN WITH ENDTIME FROM STORED ENDTIME FOR DAY
            if (minEndTimeStripped < time) {
                popLastDisabled = true;
                var newEndTimes = new Array();

                //FORMAT THE TIME BACK TO TIMEPICKER FORMAT - NO NEED FOR LOOP
                //FIRST CHECK IF TIME IS 5 LENGTH
                if (time.length === 4) {
                    time = "0" + time;
                }

                var formattingTime;
                var temp;
                temp = time.substring(0, 2);
                formattingTime = temp + ":";
                temp = time.substring(2, 4);
                formattingTime += temp + ":";
                temp = time.substring(4, 6);
                formattingTime += temp;

                //UPDATE DISABLED END TIMES
                newEndTimes.push(formattingTime);
                newEndTimes.push('24:30:00');
                dateTimes.push(newEndTimes);
                $('.et').timepicker('option', 'disableTimeRanges', dateTimes);
                break;

            }

        }

        $('.et').timepicker('option', 'disableTimeRanges', dateTimes);


    });


    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay,listWeek'
        },
        navLinks: true, // can click day/week names to navigate views
        editable: false,
        windowResize: true,
        weekends: false,
        nowIndicator: true,
        eventTextColor: 'black',
        eventBorderColor: 'black',
        views: {
            month: {
                displayEventEnd: true
            },
            agendaWeek: {
                minEndTime: '06:00:00'
            },
            agendaDay: {
                minEndTime: '06:00:00'
            },
            listWeek: {
                noEventsMessage: 'There are no meetings for this week.'
            }
        },
        eventSources: [
            urls[0], urls[1], urls[2]
                    // any other sources...
        ],
        eventClick: function (event, jsEvent, view) {
            $('#modalTitle').html("<h2>" + event.title + "</h2>");
            $('#modalTitle').append(" <em>StartTime:</em> " + event.startDisplay + "</br>");
            $('#modalTitle').append(" <em>EndTime:</em> " + event.endDisplay);
            $('#modalBody').html(event.description);
            $('#fullCalModal').modal();
        }
    });


    $("#large").change(function () {
        if ($(this).is(':checked') === false) {
            $('#calendar').fullCalendar('removeEventSource', urls[0]);
        } else {
            $('#calendar').fullCalendar('addEventSource', urls[0]);
        }
        $('#eventFilterCalendar').fullCalendar('refetchEvents');
    });

    $("#window").change(function () {
        if ($(this).is(':checked') === false) {
            $('#calendar').fullCalendar('removeEventSource', urls[1]);
        } else {
            $('#calendar').fullCalendar('addEventSource', urls[1]);

        }
        $('#eventFilterCalendar').fullCalendar('refetchEvents');
    });

    $("#small").change(function () {
        if ($(this).is(':checked') === false) {
            $('#calendar').fullCalendar('removeEventSource', urls[2]);
        } else {
            $('#calendar').fullCalendar('addEventSource', urls[2]);

        }
        $('#calendar').fullCalendar('refetchEvents');
    });

    $(function () {

        $('.btn-radio').click(function (e) {
            $('.btn-radio').not(this).removeClass('active')
                    .siblings('input').prop('checked', false)
                    .siblings('.img-radio').css('opacity', '0.5');
            $(this).addClass('active')
                    .siblings('input').prop('checked', true)
                    .siblings('.img-radio').css('opacity', '1');
            if ($('#date').val() !== "") {
                $('#form-no-date-remainder').show();
            }
        });
    });

    $('#date').click(function (e) {
        $('.st').val("");
        $('.et').val("");
        $('#form-no-date-remainder').hide();
//        dateTimes.pop();
        $('#meeting-title').val("");
        $('.btn-radio').removeClass('active').siblings('input').prop('checked', false)
                .siblings('.img-radio').css('opacity', '0.5');
//        $('.st').timepicker('option', 'disableTimeRanges', dateTimes);
//        $('.et').timepicker('option', 'disableTimeRanges', dateTimes);

    });

    $('#date').focusout(function (e) {
        $('#form-no-date').show();
    });

    $('#meeting-submit').on('click', function (e) {

        e.preventDefault();

        var formData = JSON.stringify({
            startTimePass: dateFormatter($('.st').timepicker('getTime')),
            endTimePass: dateFormatter($('.et').timepicker('getTime')),
            datePass: $('#date').val(),
            description: $('#meeting-descrip').val(),
            roomID: globalRoomChecked,
            meetingSubject: $('#meeting-title').val(),
            attendees: $('#users').val(),
            userEmail: "geoffreykfoury@gmail.com"
        });

        var email = false;

        if ($('#sendInviteEmail').is(':checked')) {
            email = true;
        }

        var token = $("meta[name='_csrf']").attr("content");
        var header = $("meta[name='_csrf_header']").attr("content");


        $.ajax({
            type: 'POST',
            url: contextRoot + "/createMeeting/" + email,
            data: formData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
                xhr.setRequestHeader(header, token);
            },
            success: function (response) {
                $('#calendar').fullCalendar('refetchEvents');
                $('#form-no-date-remainder').hide();
                $(".form-control").val('');
                $('.btn-radio').not(this).removeClass('active');
                $('#sendInviteEmail').val('');
                $('.img-radio').css('opacity', '0.5');
                $('#form-no-date').hide();
                $('#users').val('');

            }
        });
    });

    //scroll function
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({scrollTop: target.offset().top}, 1000);
                return false;
            }
        }
    });

    //navbar hide
    $(function () {
        $(window).scroll(function () {
            // set distance user needs to scroll before we fadeIn navbar
            if ($(this).scrollTop() < 700) {
                $('.navbar').fadeIn();
            } else {
                $('.navbar').fadeOut();
            }
        });
    });

    function dateFormatter(d) {

        var newDate = ('0' + d.getHours()).slice(-2) + ":" + ('0' + d.getMinutes()).slice(-2) + ":00";
        return newDate;

    }

    function oneLaterThanStart(start) {
        return new Date(start + 30 * 60000);
    }

});
