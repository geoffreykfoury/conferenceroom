$(document).ready(function () {

    users();
    groups();

    var allUsers = new Array();
    var userID;
    var selectedGroup;
    var groupTitle;

    function users() {
        $.ajax({
            type: 'GET',
            url: contextRoot + '/getUsers',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {

            $.each(data, function (index, User) {

                $('#users').append("<option value='" + User.id + "'>" + User.firstName + " " + User.lastName + " --- (" + User.email + ")</option>");
                allUsers.push(User);
            });
        });
    }


    function groups() {
        $.ajax({
            type: 'GET',
            url: contextRoot + 'addressbook/groups',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {

            $.each(data, function (index, Group) {

                $('#group-title').append("<option value='" + Group.id + "'>" + Group.title + "</option>");
            });
        });
    }


    $('#add-user').click(function (e) {
        var check = true;
        var users = $('#users').select2('data');

        $.each(users, function (i, item) {
            $('#user-list [id]').each(function () {
                if (this.id === item.id) {
                    check = false;
                    alert("Attempting to Add Duplicate User");
                    return;
                }
            });
            if (check) {
                $('#user-list').append("<li id='" + item.id + "'>" + item.text + "<a href='#' class='rm'> X </a></li>");
                $("#users option[value='" + item.id + "']").remove();
            }
        });
    });

    $(document).on("click", ".rm", function () {
        $(this).parent('li').remove();

    });

    $(document).on("click", "#reset-users", function () {
        $('#users').empty();
        users();
    });

    $('#load-users:not([disabled])').click(function (e) {

        var groupInput = $('#group-title').select2('data');
        selectedGroup = groupInput[0].id;
        groupTitle = groupInput[0].text;

        $.ajax({
            type: 'GET',
            url: contextRoot + 'addressbook/loadGroup/' + selectedGroup,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
            }
        }).success(function (data, status) {

            $.each(data, function (i, item) {
                $('#user-list').append("<li id='" + item.id + "'>" + item.firstName + " " + item.lastName + " --- (" + item.email + ")  " + "<a href='#' class='rm'> X </a></li>");
                $("#users option[value='" + item.id + "']").remove();
            });
            $('#load-users').addClass('disabled');
            $('#group-title').val("");
            $('#group-title').attr('disabled', true);
        });
    });


    $('#update-group').click(function (e) {

        var allUsersIDs = new Array();
        $('#user-list [id]').each(function () {
            allUsersIDs.push(this.id);
        });

        var updateGroupData = JSON.stringify({
            users: allUsersIDs,
            userID: 1,
            id: selectedGroup,
            title: groupTitle
        });

        $.ajax({
            type: 'POST',
            url: contextRoot + "/addressbook/updateGroup",
            data: updateGroupData,
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-type", "application/json");
            },
            success: function (response) {

                $('#load-users').removeClass('disabled');
                $('#group-title').attr('disabled', false);


            }
        });

    });



});