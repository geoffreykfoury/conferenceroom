/*
 * The MIT License
 *
 * Copyright 2017 Pivotal Software, Inc..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.confroom.controllers;

import com.confroom.DAO.UserDAO;
import com.confroom.models.Group;
import com.confroom.models.Meeting;
import com.confroom.models.User;
import java.text.ParseException;
import java.util.List;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author gkfoury
 */
@Controller
public class AddressBookController {

    @Autowired
    UserDAO userDao;

    @RequestMapping("/addressbook")
    public String landingPage(Model model) throws MessagingException, ParseException {

        return "address-book";
    }

    @ResponseBody
    @RequestMapping("/addressbook/groups")
    public List<Group> getGroups() throws MessagingException, ParseException {

        List<Group> groupsOwned = userDao.getOwnerGroups(1);

        return groupsOwned;
    }

    @ResponseBody
    @RequestMapping("/addressbook/loadGroup/{groupID}")
    public List<User> loadGroup(@PathVariable int groupID) throws MessagingException, ParseException {

        List<User> groupUsers = userDao.getUsersGroups(groupID);

        return groupUsers;
    }

    @ResponseBody
    @RequestMapping("/addressbook/updateGroup")
    public Boolean updateGroup(@RequestBody Group group) throws MessagingException, ParseException {
        
        userDao.updateGroup(group);

        return true;

    }

}
