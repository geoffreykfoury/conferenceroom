package com.confroom.controllers;

import com.confroom.DAO.MainDAO;
import com.confroom.DAO.UserDAO;
import com.confroom.models.CalendarMeeting;
import com.confroom.models.Meeting;
import com.confroom.models.RestrictedDates;
import com.confroom.models.User;
import com.confroom.services.EmailService;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javax.mail.MessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {

    //Should be using Service to contact DAO and manipulate data.  Following rules to break rules. love it.
    @Autowired
    MainDAO mainDao;
    @Autowired
    UserDAO userDao;
    @Autowired
    EmailService emailService;

    @RequestMapping("/")
    public String landingPage() throws MessagingException, ParseException {
        return "home";
    }

    @RequestMapping("/login")
    public String login(Model model) {
        model.addAttribute("user", new User());
        return "login";
    }

    @RequestMapping("/login-error")
    public String loginError(Model model) {
        model.addAttribute("loginError", true);
        return "login";
    }

    @RequestMapping("/date/{date}/{roomID}")
    @ResponseBody
    public List<RestrictedDates> checkAvailabeDates(@PathVariable("date") String selectedDate, @PathVariable("roomID") int roomID) {

        LocalDate date = LocalDate.parse(selectedDate);

        List<RestrictedDates> restrictedDates = mainDao.getMeetingTimes(date, roomID);

        return restrictedDates;
    }

    @RequestMapping("/large")
    @ResponseBody
    public List<CalendarMeeting> largeResults(@RequestParam("start") String start, @RequestParam("end") String end) {

        List<Meeting> allMeetings = mainDao.getMeetingsLarge(start, end);
        List<CalendarMeeting> cms = new ArrayList();

        for (Meeting mt : allMeetings) {
            CalendarMeeting cm = new CalendarMeeting();
            cm.setTitle(mt.getMeetingSubject());
            cm.setStart(mt.getDate().toString() + "T" + mt.getStartTime().toString());
            cm.setEnd(mt.getDate().toString() + "T" + mt.getEndTime().toString());
            cm.setBackgroundColor("#38AD57");
            cm.setStartDisplay(mt.getStartTime().toString());
            cm.setEndDisplay(mt.getEndTime().toString());
            cm.setDescription(mt.getDescription());
            cms.add(cm);
        }
        return cms;

    }

    @RequestMapping("/window")
    @ResponseBody
    public List<CalendarMeeting> windowResults(@RequestParam("start") String start, @RequestParam("end") String end) {

        List<Meeting> allMeetings = mainDao.getMeetingsWindow(start, end);
        List<CalendarMeeting> cms = new ArrayList();

        for (Meeting mt : allMeetings) {
            CalendarMeeting cm = new CalendarMeeting();
            cm.setTitle(mt.getMeetingSubject());
            cm.setStart(mt.getDate().toString() + "T" + mt.getStartTime().toString());
            cm.setEnd(mt.getDate().toString() + "T" + mt.getEndTime().toString());
            cm.setBackgroundColor("#FFC706");
            cm.setStartDisplay(mt.getStartTime().toString());
            cm.setEndDisplay(mt.getEndTime().toString());
            cm.setDescription(mt.getDescription());
            cms.add(cm);
        }
        return cms;
    }

    @RequestMapping("/small")
    @ResponseBody
    public List<CalendarMeeting> smallResults(@RequestParam("start") String start, @RequestParam("end") String end) {

        List<Meeting> allMeetings = mainDao.getMeetingsSmall(start, end);
        List<CalendarMeeting> cms = new ArrayList();

        for (Meeting mt : allMeetings) {
            CalendarMeeting cm = new CalendarMeeting();
            cm.setTitle(mt.getMeetingSubject());
            cm.setStart(mt.getDate().toString() + "T" + mt.getStartTime().toString());
            cm.setEnd(mt.getDate().toString() + "T" + mt.getEndTime().toString());
            cm.setBackgroundColor("#16C1FF");
            cm.setStartDisplay(mt.getStartTime().toString());
            cm.setEndDisplay(mt.getEndTime().toString());
            cm.setDescription(mt.getDescription());
            cms.add(cm);
        }
        return cms;
    }

    @RequestMapping(value = "/createMeeting/{email}", method = RequestMethod.POST)
    @ResponseBody
    public Meeting createMeeting(@RequestBody Meeting newMeeting, @PathVariable("email") Boolean email) {

        newMeeting.setStartTime(LocalTime.parse(newMeeting.getStartTimePass()));
        newMeeting.setEndTime(LocalTime.parse(newMeeting.getEndTimePass()));
        newMeeting.setDate(LocalDate.parse(newMeeting.getDatePass()));

        //Handle no attendees option from JQuery and lack of ArrayList initialization
        if (newMeeting.getAttendees() == null) {
            ArrayList<String> attendees = new ArrayList<String>();
            attendees.add("filler - not null");
            newMeeting.setAttendees(attendees);
        }

        mainDao.createMeeting(newMeeting);

        if (email) {
            emailService.createCalInvite(newMeeting);
        }

        return newMeeting;
    }

    @RequestMapping(value = "/getUsers", method = RequestMethod.GET)
    @ResponseBody
    public List<User> sendInvite() {
        return userDao.getAllUsers();
    }

}
