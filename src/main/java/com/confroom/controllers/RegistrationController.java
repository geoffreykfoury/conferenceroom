/*
 * The MIT License
 *
 * Copyright 2017 Pivotal Software, Inc..
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.confroom.controllers;

import com.confroom.DAO.RegistrationDAO;
import com.confroom.DAO.UserDAO;
import com.confroom.models.User;
import com.confroom.models.UserRegister;
import com.confroom.models.VerificationToken;
import com.confroom.services.UserService;
import com.confroom.tools.RegistrationEmail;
import java.sql.Timestamp;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author gkfoury
 */
@Controller
public class RegistrationController {

    @Autowired
    RegistrationDAO registrationDAO;

    @Autowired
    RegistrationEmail registrationEmail;

    @Autowired
    UserDAO userDAO;

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;


    //This method is fired when the user first signs up.  creates a disabled user
    @RequestMapping(value = "/registration/create", method = RequestMethod.POST)
    public String createUser(Model model, @ModelAttribute User user, @RequestParam("password") String password) {

        String webPageResult = "confirm";

        UserRegister newUser = new UserRegister();

        password = passwordEncoder.encode(password);

        try {
            newUser.setPassword(password);
            newUser.setUsername(user.getEmail());
            String userID = registrationDAO.newUser(newUser);
            VerificationToken newToken = new VerificationToken();
            String randToken = UUID.randomUUID().toString();
            newToken.setUserID(userID);
            newToken.setToken(randToken);
            userDAO.addNewUser(user);
            Timestamp expiration = userService.getExpirationDate();

            newToken.setExpiration(expiration);

            registrationDAO.addRegToken(newToken);

            registrationEmail.sendRegistrationEmail(user.getEmail(), randToken);

            String message = "A confirmation email has been sent to " + userID;
            model.addAttribute("confirmMessage", message);
        } 
        catch (DataAccessException ex) {
            ex.printStackTrace();
            webPageResult = "login";
        }catch (java.lang.Exception ex) {
            ex.printStackTrace();
            webPageResult = "login";
        }
        
        return webPageResult;

    }

    //This is launched once the link has been navigated to.  This verfies the user and token match
    @RequestMapping(value = "/registration/verify")
    public String verifyNewUser(@RequestParam(value = "token") String token, Model model) {

        String userId = registrationDAO.addRegToken(token);

        if (userId != null && !userId.isEmpty()) {
            userDAO.enableUser(userId);
            registrationDAO.deleteToken(userId);
            model.addAttribute("validMessage", "Thanks for confirming!  You are now a registered user.");
        } else {
            model.addAttribute("validMessage", "We did not recognize your token.  Are you sure you followed the correct registration link?");
        }
        return "registration-link";
    }

}
