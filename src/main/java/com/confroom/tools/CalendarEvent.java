package com.confroom.tools;

import java.io.IOException;
import java.text.ParseException;
import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

/**
 *
 * @author gkfoury
 */
@Service
public class CalendarEvent {

    private JavaMailSender javaMailSender;

    @Autowired
    public CalendarEvent(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void createCal(String email, CalendarInvite invite) throws MessagingException, ParseException, IOException {

//        CI.convertDates(date, endDate);
        StringBuffer sb = new StringBuffer();

        StringBuffer buffer = sb.append(
                "BEGIN:VCALENDAR\n"
                + "PRODID:-//Microsoft Corporation//Outlook 9.0 MIMEDIR//EN\n"
                + "VERSION:2.0\n"
                + "METHOD:REQUEST\n"
                + "BEGIN:VTIMEZONE\n"
                + "TZID:America/New_York\n"
                + "X-LIC-LOCATION:America/New_York\n"
                + "BEGIN:STANDARD\n"
                + "DTSTART:20071104T020000\n"
                + "TZOFFSETFROM:-0400\n"
                + "TZOFFSETTO:-0500\n"
                + "TZNAME:EST\n"
                + "END:STANDARD\n"
                + "BEGIN:DAYLIGHT\n"
                + "DTSTART:20070311T020000\n"
                + "TZOFFSETFROM:-0500\n"
                + "TZOFFSETTO:-0400\n"
                + "TZNAME:EDT\n"
                + "END:DAYLIGHT\n"
                + "END:VTIMEZONE\n"
                + "BEGIN:VEVENT\n"
                + "ATTENDEE;ROLE=REQ-PARTICIPANT;RSVP=TRUE:MAILTO:" + email + "\n"
                + "ORGANIZER:MAILTO:" + invite.getUserEmail() + "\n"
                + "DTSTART;TZID=America/New_York:" + invite.getDateStart() + "\n"
                + "DTEND;TZID=America/New_York:" + invite.getDateEnd() + "\n"
                + "LOCATION:" + invite.getRoom() + "\n"
                + "TRANSP:OPAQUE\n"
                + "SEQUENCE:0\n"
                + "UID:" + invite.getUID() + "\n"
                + "DTSTAMP:20160101T120102Z\n"
                + "CATEGORIES:Meeting\n"
                + "DESCRIPTION:" + invite.getDescription() + "\n"
                + "SUMMARY:" + invite.getDescription() + "\n"
                + "PRIORITY:5\n"
                + "CLASS:PUBLIC\n"
                + "BEGIN:VALARM\n"
                + "TRIGGER:PT1440M\n"
                + "ACTION:DISPLAY\n"
                + "DESCRIPTION:Reminder\n"
                + "END:VALARM\n"
                + "END:VEVENT\n"
                + "END:VCALENDAR");

        MimeMessage message = javaMailSender.createMimeMessage();
        message.addHeaderLine("charset=UTF-8");
        message.addHeaderLine("component=VEVENT");
        message.addHeaderLine("method=REQUEST");

        message.setFrom("geoffrey.kfoury@ionep.com");
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
        message.setSubject(invite.getSubject());

        BodyPart messageBodyPart = new MimeBodyPart();

        messageBodyPart.setHeader("Content-Class", "urn:content-classes:calendarmessage");
        messageBodyPart.setHeader("Content-ID", "calendar_message");
        messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(buffer.toString(), "text/calendar")));

        Multipart multipart = new MimeMultipart();

        multipart.addBodyPart(messageBodyPart);

        message.setContent(multipart);

        javaMailSender.send(message);

    }

    //Obviously a cleaner method but does not play nicely with Outlook and ICS. This method works with GMAIL.
    //************************************************************************************************************
    
//    public void createCalNotOutlook() throws MessagingException, ParseException, IOException {
//        ICalendar ical = new ICalendar();
//
//        VEvent event = new VEvent();
//
//        Attendee attendee = new Attendee("Geoff", "geoffrey.kfoury@ionep.com");
//        attendee.setRsvp(true);
//        attendee.setRole(Role.ATTENDEE);
//        attendee.setParticipationLevel(ParticipationLevel.OPTIONAL);
//
//        event.addAttendee(attendee);
//        event.setSummary("yo");
//
//        DateTime dt = new DateTime(2016, 12, 28, 14, 0);
//        DateTime et = new DateTime(2016, 12, 28, 17, 30);
//        Date starts = (Date) dt.toDate();
//        Date ends = (Date) et.toDate();
//
//        DateStart thisStart = new DateStart(starts, true);
//        DateEnd dateEnd = new DateEnd(ends, true);
//
//        event.setDateStart(thisStart);
//        event.setDateEnd(dateEnd);
//
//        Duration reminder = new Duration.Builder().minutes(15).build();
//        Trigger trigger = new Trigger(reminder, Related.START);
//        Action action = new Action("DISPLAY");
//        VAlarm valarm = new VAlarm(action, trigger);
//        event.addAlarm(valarm);
//
//        Duration duration = new Duration.Builder().hours(1).build();
//        event.setDuration(duration);
//
//        event.setUid("GeoffreyKfoury");
//        event.setOrganizer("geoffrey.kfoury@ionep.com");
//        event.setLocation("Small");
//
//        ical.addEvent(event);
//
//        String str = Biweekly.write(ical).go();
//
//        MimeMessage message = javaMailSender.createMimeMessage();
//        message.addHeaderLine("charset=UTF-8");
//        message.addHeaderLine("component=VEVENT");
//        message.addHeaderLine("method=REQUEST");
//
//        message.setFrom("geoffrey.kfoury@ionep.com");
//        message.addRecipient(Message.RecipientType.TO, new InternetAddress("geoffrey.kfoury@ionep.com"));
//        message.setSubject("You're Invited to a Meeting");
//
//        BodyPart messageBodyPart = new MimeBodyPart();
//
//        messageBodyPart.setHeader("Content-Class", "urn:content-classes:calendarmessage");
//        messageBodyPart.setHeader("Content-ID", "calendar_message");
//        messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(str.toString(), "text/calendar")));
//
//        Multipart multipart = new MimeMultipart();
//
//        multipart.addBodyPart(messageBodyPart);
//
//        messageBodyPart = new MimeBodyPart();
//        String filename = "How are you.ics";
//        messageBodyPart.setFileName(filename);
//        messageBodyPart.setDataHandler(new DataHandler(new ByteArrayDataSource(str, "text/calendar")));
//
//        messageBodyPart.setContent(str, "text/plain");
//
//        multipart.addBodyPart(messageBodyPart);
//
//        message.setContent(multipart);
//
//        javaMailSender.send(message);
//
//    }
}
