package com.confroom.tools;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class RegistrationEmail {

    private JavaMailSender javaMailSender;

    @Autowired
    public RegistrationEmail(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public String sendRegistrationEmail(String username, String tokenKey) {

        try {
            MimeMessage regEmail = javaMailSender.createMimeMessage();

            MimeMessageHelper emailVerify = new MimeMessageHelper(regEmail, true);

            emailVerify.setTo(username);

            emailVerify.setFrom("geoffrey.kfoury@ionep.com");

//            String htmlHeader = "<html xmlns:v=3D'urn:schemas-microsoft-com:vml' xmlns:o=3D'urn:schemas-micr=osoft-com:office:office' xmlns:w=3D'urn:schemas-microsoft-com:office:word' =xmlns:m=3D'http://schemas.microsoft.com/office/2004/12/omml' xmlns=3D'http:=//www.w3.org/TR/REC-html40'>";
//            String head = "<head>\n"
//                    + "<meta http-equiv=3D\"Content-Type\" content=3D\"text/html; charset=3Dus-ascii\"=\n"
//                    + ">\n"
//                    + "<meta name=3D\"Generator\" content=3D\"Microsoft Word 15 (filtered medium)\">\n"
//                    + "<style><!--\n"
//                    + "/* Font Definitions */\n"
//                    + "@font-face\n"
//                    + "=09{font-family:\"Cambria Math\";\n"
//                    + "=09panose-1:2 4 5 3 5 4 6 3 2 4;}\n"
//                    + "@font-face\n"
//                    + "=09{font-family:Calibri;\n"
//                    + "=09panose-1:2 15 5 2 2 2 4 3 2 4;}\n"
//                    + "/* Style Definitions */\n"
//                    + "p.MsoNormal, li.MsoNormal, div.MsoNormal\n"
//                    + "=09{margin:0in;\n"
//                    + "=09margin-bottom:.0001pt;\n"
//                    + "=09font-size:11.0pt;\n"
//                    + "=09font-family:\"Calibri\",sans-serif;}\n"
//                    + "a:link, span.MsoHyperlink\n"
//                    + "=09{mso-style-priority:99;\n"
//                    + "=09color:#0563C1;\n"
//                    + "=09text-decoration:underline;}\n"
//                    + "a:visited, span.MsoHyperlinkFollowed\n"
//                    + "=09{mso-style-priority:99;\n"
//                    + "=09color:#954F72;\n"
//                    + "=09text-decoration:underline;}\n"
//                    + "span.EmailStyle17\n"
//                    + "=09{mso-style-type:personal-compose;\n"
//                    + "=09font-family:\"Calibri\",sans-serif;\n"
//                    + "=09color:windowtext;}\n"
//                    + ".MsoChpDefault\n"
//                    + "=09{mso-style-type:export-only;\n"
//                    + "=09font-family:\"Calibri\",sans-serif;}\n"
//                    + "@page WordSection1\n"
//                    + "=09{size:8.5in 11.0in;\n"
//                    + "=09margin:1.0in 1.0in 1.0in 1.0in;}\n"
//                    + "div.WordSection1\n"
//                    + "=09{page:WordSection1;}\n"
//                    + "--></style><!--[if gte mso 9]><xml>\n"
//                    + "<o:shapedefaults v:ext=3D\"edit\" spidmax=3D\"1026\" />\n"
//                    + "</xml><![endif]--><!--[if gte mso 9]><xml>\n"
//                    + "<o:shapelayout v:ext=3D\"edit\">\n"
//                    + "<o:idmap v:ext=3D\"edit\" data=3D\"1\" />\n"
//                    + "</o:shapelayout></xml><![endif]-->\n"
//                    + "</head>";
//            String body = "<body lang=3D\"EN-US\" link=3D\"#0563C1\" vlink=3D\"#954F72\">";
//            emailVerify.setText(htmlHeader + head + body + "<div class=3D\"WordSection1\"><p class=3D\"MsoNormal\"><a href=3D\"localhost:9090/user/verify?token=" + tokenKey + "\">Click me to continue registration</a=><o:p></o:p></p></div></body></html>", true);
            String body = "<div>\n"
                    + "<!--[if mso]>\n"
                    + "  <v:roundrect xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" href=\"http://localhost:9090/registration/verify?token=" + tokenKey + "\" style=\"height:40px;v-text-anchor:middle;width:300px;\" arcsize=\"10%\" stroke=\"f\" fillcolor=\"#d62828\">\n"
                    + "    <w:anchorlock/>\n"
                    + "    <center style=\"color:#ffffff;font-family:sans-serif;font-size:16px;font-weight:bold;\">\n"
                    + "      Finish Your Registration Here!\n"
                    + "    </center>\n"
                    + "  </v:roundrect>\n"
                    + "  <![endif]-->\n"
                    + "  <![if !mso]>\n"
                    + "  <table cellspacing=\"0\" cellpadding=\"0\"> <tr> \n"
                    + "  <td align=\"center\" width=\"300\" height=\"40\" bgcolor=\"#d62828\" style=\"-webkit-border-radius: 5px; -moz-border-radius: 5px; border-radius: 5px; color: #ffffff; display: block;\">\n"
                    + "    <a  href=\"http://localhost:9090/registration/verify?token=" + tokenKey + "\" style=\"font-size:16px; font-weight: bold; font-family:sans-serif; text-decoration: none; line-height:40px; width:100%; display:inline-block\">\n"
                    + "    <span style=\"color: #ffffff;\">\n"
                    + "      Finish Your Registration Here!\n"
                    + "    </span>\n"
                    + "    </a>\n"
                    + "  </td> \n"
                    + "  </tr> </table> \n"
                    + "  <![endif]>\n"
                    + "</div>";

            emailVerify.setText(body, true);

//                // use the true flag to indicate the text included is HTML
//                helper.setText("<html><body><img src='cid:identifier1234'></body></html>", true);
//
//                // let's include the infamous windows Sample file (this time copied to c:/)
//                FileSystemResource res = new FileSystemResource(new File("c:/Sample.jpg"));
//                helper.addInline("identifier1234", res)
            javaMailSender.send(regEmail);

        } catch (MessagingException ex) {
            Logger.getLogger(RegistrationEmail.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

}
