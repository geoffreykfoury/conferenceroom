package com.confroom.services;

import com.confroom.models.Meeting;
import com.confroom.tools.CalendarEvent;
import com.confroom.tools.CalendarInvite;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.MessagingException;
import microsoft.exchange.webservices.data.core.ExchangeService;
import microsoft.exchange.webservices.data.core.enumeration.misc.ExchangeVersion;
import microsoft.exchange.webservices.data.core.service.item.EmailMessage;
import microsoft.exchange.webservices.data.credential.ExchangeCredentials;
import microsoft.exchange.webservices.data.credential.WebCredentials;
import microsoft.exchange.webservices.data.property.complex.MessageBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailService {

    @Autowired
    CalendarInvite CI;

    @Autowired
    CalendarEvent CE;

    public void createCalInvite(Meeting meeting) {

        // Always same date - Strip non-digits
        String date = stripNonDigits(meeting.getDatePass());
        // Concat dates and time for .ics format 
        // Strip non-digits
        String startDate = date + "T" + stripNonDigits(meeting.getStartTimePass());
        String endDate = date + "T" + stripNonDigits(meeting.getEndTimePass());

        // Create unique UID
        String UID = createUID(startDate, endDate, meeting.getRoomID());

        //Sneaky way of getting room name.  Totally not ideal but being aware of only three rooms
        switch (meeting.getRoomID()) {

            case 1:
                CI.setRoom("Window");
                break;
            case 2:
                CI.setRoom("Large");
                break;
            case 3:
                CI.setRoom("Small");
                break;
        }

        CI.setDateStart(startDate);
        CI.setDateEnd(endDate);
        CI.setUID(UID);
//        CI.setUserID(meeting.getUserEmail());
        CI.setSubject(meeting.getMeetingSubject());
        CI.setDescription(meeting.getDescription());
        CI.setUserEmail(meeting.getUserEmail());

        sendEmail(meeting.getAttendees(), CI);

    }

    public String createUID(String start, String end, int room) {
        String UID = start + room + end;
        return UID;
    }

    public void sendEmail(ArrayList<String> emails, CalendarInvite invite) {

        try {
            CE.createCal(emails.get(0), invite);
        } catch (MessagingException | ParseException | IOException ex) {
            Logger.getLogger(EmailService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String stripNonDigits(String toStrip) {

        return toStrip.replaceAll("\\D+", "");

    }

    public void sendEmailOfficeAPI(){


    }

}
