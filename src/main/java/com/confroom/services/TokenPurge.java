package com.confroom.services;

import com.confroom.DAO.RegistrationDAO;
import com.confroom.DAO.UserDAO;
import com.confroom.models.VerificationToken;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 *
 * @author gkfoury
 */
@Component
public class TokenPurge {

    @Autowired
    RegistrationDAO registrationDAO;

    @Autowired
    UserDAO userDAO;

    @Scheduled(fixedRate = 3600000)
    public void purgeTokens() {
        
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());

        List<VerificationToken> tokens = registrationDAO.getAllTokens();

        for (ListIterator<VerificationToken> iter = tokens.listIterator(); iter.hasNext();) {
           VerificationToken element = iter.next();
           
           if (element.getExpiration().before(currentTime)){
               
               registrationDAO.deleteToken(element.getUserID());
               registrationDAO.deleteAuthority(element.getUserID());
               userDAO.deleteUser(element.getUserID());
           }
           
        }


    }
}
