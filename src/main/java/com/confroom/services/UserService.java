package com.confroom.services;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    public Timestamp getExpirationDate() {
        
        LocalDateTime expires = LocalDateTime.now().plusHours(24);

        Timestamp expireDate = Timestamp.valueOf(expires);

//        DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");

        return expireDate ;

    }

}
