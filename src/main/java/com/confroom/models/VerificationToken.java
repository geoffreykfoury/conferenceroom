package com.confroom.models;

import java.sql.Timestamp;


/**
 *
 * @author Geoffrey
 */
public class VerificationToken {

    private String token;
    private Timestamp expiration;
    private String userID;

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the expiration
     */
    public Timestamp getExpiration() {
        return expiration;
    }

    /**
     * @param expiration the expiration to set
     */
    public void setExpiration(Timestamp expiration) {
        this.expiration = expiration;
    }

    /**
     * @return the userID
     */
    public String getUserID() {
        return userID;
    }

    /**
     * @param userID the userID to set
     */
    public void setUserID(String userID) {
        this.userID = userID;
    }
    

}
