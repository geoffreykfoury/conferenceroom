/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.confroom.models;

/**
 *
 * @author gkfoury
 */
public class CalendarMeeting {

    private String title;
    private boolean allDay;
    private String start;
    private String end;
    private String backgroundColor;
    private String textColor;
    private String description;
    private String startDisplay;
    private String endDisplay;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**

    /**
     * @return the start
     */
    public String getStart() {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(String start) {
        this.start = start;
    }

    /**
     * @return the end
     */
    public String getEnd() {
        return end;
    }

    /**
     * @param end the end to set
     */
    public void setEnd(String end) {
        this.end = end;
    }

    /**
     * @return the backgroundColor
     */
    public String getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * @param backgroundColor the backgroundColor to set
     */
    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * @return the allDay
     */
    public boolean isAllDay() {
        return allDay;
    }

    /**
     * @param allDay the allDay to set
     */
    public void setAllDay(boolean allDay) {
        this.allDay = allDay;
    }
    
    /**
     * @return the textColor
     */
    public String getTextColor() {
        return textColor;
    }

    /**
     * @param textColor the textColor to set
     */
    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }
    
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the startDisplay
     */
    public String getStartDisplay() {
        return startDisplay;
    }

    /**
     * @param startDisplay the startDisplay to set
     */
    public void setStartDisplay(String startDisplay) {
        this.startDisplay = startDisplay;
    }

    /**
     * @return the endDisplay
     */
    public String getEndDisplay() {
        return endDisplay;
    }

    /**
     * @param endDisplay the endDisplay to set
     */
    public void setEndDisplay(String endDisplay) {
        this.endDisplay = endDisplay;
    }

}
