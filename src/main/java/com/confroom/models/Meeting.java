/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.confroom.models;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 *
 * @author gkfoury
 */
public class Meeting {

    private int ID;
    private int userID;
    private int roomID; 
    private String MeetingRoom;
    
    private ArrayList<String> attendees;
    private String meetingSubject;
    private String Description;
    private String userEmail;

    private LocalDate date;
    private LocalTime startTime;
    private LocalTime endTime;

    private String datePass;
    private String startTimePass;
    private String endTimePass;
    


    public Meeting() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
  
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getDatePass() {
        return datePass;
    }

    public void setDatePass(String datePass) {
        this.datePass = datePass;
    }

    public String getStartTimePass() {
        return startTimePass;
    }

    public void setStartTimePass(String startTimePass) {
        this.startTimePass = startTimePass;
    }

    public String getEndTimePass() {
        return endTimePass;
    }

    public void setEndTimePass(String endTimePass) {
        this.endTimePass = endTimePass;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public ArrayList<String> getAttendees() {
        return attendees;
    }

    public void setAttendees(ArrayList attendees) {
        this.attendees = attendees;
    }

    public String getMeetingSubject() {
        return meetingSubject;
    }

    public void setMeetingSubject(String meetingSubject) {
        this.meetingSubject = meetingSubject;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    /**
     * @return the MeetingRoom
     */
    public String getMeetingRoom() {
        return MeetingRoom;
    }

    /**
     * @param MeetingRoom the MeetingRoom to set
     */
    public void setMeetingRoom(String MeetingRoom) {
        this.MeetingRoom = MeetingRoom;
    }

}
