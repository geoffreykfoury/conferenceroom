/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.confroom.DAO;

import com.confroom.models.Meeting;
import com.confroom.models.RestrictedDates;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class MainDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    RoomDAO roomDAO;

    @Autowired
    User_Meeting_DAO umDAO;

    private final String GET_MEETING_LARGE = "SELECT * FROM conference.meeting where meetingDate >= str_to_date(?, '%Y-%m-%d') AND  meetingDate <= str_to_date(?, '%Y-%m-%d') AND roomID = 2";
    private final String GET_MEETING_WINDOW = "SELECT * FROM conference.meeting where meetingDate >= str_to_date(?, '%Y-%m-%d') AND  meetingDate <= str_to_date(?, '%Y-%m-%d') AND roomID = 1";
    private final String GET_MEETING_SMALL = "SELECT * FROM conference.meeting where meetingDate >= str_to_date(?, '%Y-%m-%d') AND  meetingDate <= str_to_date(?, '%Y-%m-%d') AND roomID = 3";
    private final String GET_MEETING_TIMES = "select startTime, endTime from meeting where meetingDate = ? AND roomID = ?";
    private final String CREATE_NEW_MEETING = "insert into conference.meeting set description=?, meetingDate=?, userID=?, roomID=?, meetingSubject=?, startTime=?, endTime=?";

    public Meeting createMeeting(Meeting meeting) {
        jdbcTemplate.update(CREATE_NEW_MEETING, meeting.getDescription(), meeting.getDate(),
                meeting.getUserID(), meeting.getRoomID(),
                meeting.getMeetingSubject(), meeting.getStartTime(),
                meeting.getEndTime());

        int newId = jdbcTemplate.queryForObject("select last_insert_id()", Integer.class);
        meeting.setID(newId);

        if (!meeting.getAttendees().get(0).equals("filler - not null")) {
            meeting.getAttendees().stream().forEach((email) -> {
                umDAO.syncWithMeetingCreate(newId, email);
            });
        }
        return meeting;
    }

    public List<RestrictedDates> getMeetingTimes(LocalDate date, int roomID) {

        List<RestrictedDates> meetingTimes = jdbcTemplate.query(GET_MEETING_TIMES, new MeetingTimeMapper(), date, roomID);

        return meetingTimes;

    }

    public List getMeetingsLarge(String start, String end) {

        List<Meeting> meetings = jdbcTemplate.query(GET_MEETING_LARGE, new MeetingMapper(), start, end);

        return meetings;
    }

    public List getMeetingsWindow(String start, String end) {

        List<Meeting> meetings = jdbcTemplate.query(GET_MEETING_WINDOW, new MeetingMapper(), start, end);

        return meetings;
    }

    public List getMeetingsSmall(String start, String end) {

        List<Meeting> meetings = jdbcTemplate.query(GET_MEETING_SMALL, new MeetingMapper(), start, end);

        return meetings;
    }

    private class MeetingTimeMapper implements RowMapper<RestrictedDates> {

        public MeetingTimeMapper() {
        }

        @Override
        public RestrictedDates mapRow(ResultSet rs, int i) throws SQLException {

            RestrictedDates rd = new RestrictedDates();

            rd.setStartTime(rs.getTime("startTime").toString());
            rd.setEndTime(rs.getTime("endTime").toString());

            return rd;
        }
    }

    private class MeetingMapper implements RowMapper<Meeting> {

        public MeetingMapper() {
        }

        @Override
        public Meeting mapRow(ResultSet rs, int i) throws SQLException {

            Meeting thisMeeting = new Meeting();

            LocalDate startDate = rs.getDate("meetingDate").toLocalDate();
            LocalTime startTime = rs.getTime("startTime").toLocalTime();
            LocalTime endTime = rs.getTime("endTime").toLocalTime();

            thisMeeting.setStartTime(startTime);
            thisMeeting.setEndTime(endTime);
            thisMeeting.setDate(startDate);

            thisMeeting.setID(rs.getInt("scheduleID"));
            thisMeeting.setMeetingSubject(rs.getString("meetingSubject"));
            thisMeeting.setDescription(rs.getString("description"));

            thisMeeting.setMeetingRoom(roomDAO.getRoomName(rs.getInt("roomID")).getRoomName());

            return thisMeeting;

        }

    }
}
