/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.confroom.DAO;

import com.confroom.models.Group;
import com.confroom.models.User;
import com.confroom.models.UserRegister;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gkfoury
 */
@Repository
public class UserDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String ADD_NEW_USER = "insert into userinfo set firstName =?, lastName=?, phone=?, email=?";

    private final String GET_ALL_USERS = "select firstName, lastName, email, userID from conference.userinfo";
    private final String GET_USER_GROUPS = "SELECT userinfo.userID, userinfo.firstName, userinfo.lastName, userinfo.email from user_group "
            + "inner join userinfo on user_group.userID = userinfo.userID "
            + "inner join groups on user_group.groupID = groups.id where groups.id = ?";
    private final String GET_GROUP_OWNER = "select * from groups where user_id = ?";

    private final String GET_MEETINGS_INVITED = "SELECT meetingSubject, startTime, endTime, meetingDate, description FROM meeting "
            + "INNER JOIN user_meeting ON meeting.scheduleID = user_meeting.scheduleID where user_meeting.userID = ?";

    private final String DELETE_GROUP_BY_ID = "delete from groups where id = ?";
    private final String CREATE_NEW_GROUP = "insert into groups set user_id = ?, group_title=?";
    private final String ADD_USER_TO_GROUP = "insert into user_group set userID = ?, groupID = ?";

    private final String ENABLE_USER = "update users set enabled='true' where username=?";
    private final String DISABLE_USER = "update users set enabled='true' where username=?";

    private final String DELETE_USER = "delete from users where username = ?";
    private final String DELETE_USER_INFO = "delete from userinfo where username = ?";

    public void addNewUser(User user) {

        jdbcTemplate.update(ADD_NEW_USER, user.getfirstName(), user.getLastName(), user.getPhone(), user.getEmail());

    }

    public List getAllUsers() {

        List<User> users = jdbcTemplate.query(GET_ALL_USERS, new UserDAO.UserMapper());

        return users;
    }

    public List<Group> getOwnerGroups(int userID) {

        List<Group> groups = jdbcTemplate.query(GET_GROUP_OWNER, new UserDAO.GroupMapper(), userID);

        return groups;
    }

    public List getUsersGroups(int groupID) {

        List<User> users = jdbcTemplate.query(GET_USER_GROUPS, new UserDAO.UserMapper(), groupID);

        return users;
    }

    public boolean updateGroup(Group group) {

        jdbcTemplate.update(DELETE_GROUP_BY_ID, group.getId());
        jdbcTemplate.update(CREATE_NEW_GROUP, group.getUserID(), group.getTitle());

        int newGroupId = jdbcTemplate.queryForObject("select last_insert_id()", Integer.class);

        for (int userID : group.getUsers()) {
            jdbcTemplate.update(ADD_USER_TO_GROUP, userID, newGroupId);
        }

        return true;
    }

    public String enableUser(String username) {

        String result = "success";

        try {
            jdbcTemplate.update(ENABLE_USER, username);
        } catch (DataAccessException e) {
            result = "Error enabling user";
        }

        return result;
    }

    public String deleteUser(String username) {

        String result = "success";

        try {
            jdbcTemplate.update(DELETE_USER, username);
        } catch (DataAccessException e) {
            result = "Error deleting user";
        }

        return result;

    }

    private class UserMapper implements RowMapper<User> {

        public UserMapper() {
        }

        @Override
        public User mapRow(ResultSet rs, int i) throws SQLException {

            User newUser = new User();

            newUser.setfirstName(rs.getString("firstName"));
            newUser.setLastName(rs.getString("lastName"));
            newUser.setEmail(rs.getString("email"));
            newUser.setId(rs.getInt("userID"));

            return newUser;

        }
    }

    private class GroupMapper implements RowMapper<Group> {

        public GroupMapper() {
        }

        @Override
        public Group mapRow(ResultSet rs, int i) throws SQLException {

            Group newGroup = new Group();
            newGroup.setId(rs.getInt("id"));
            newGroup.setUserID(rs.getInt("user_id"));
            newGroup.setTitle(rs.getString("group_title"));
            return newGroup;
        }
    }

}
