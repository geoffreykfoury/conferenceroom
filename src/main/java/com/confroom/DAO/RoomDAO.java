/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.confroom.DAO;

import com.confroom.models.Room;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class RoomDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String GET_ROOM_NAME = "select name from rooms where roomID = ?";

    public Room getRoomName(int roomID) {
        
       Room thisRoom = jdbcTemplate.queryForObject(GET_ROOM_NAME, new RoomMapper(), roomID);
       
       return thisRoom;

    }

    private class RoomMapper implements RowMapper<Room> {

        public RoomMapper() {
        }

        @Override
        public Room mapRow(ResultSet rs, int i) throws SQLException {

            Room thisRoom = new Room();
            
            thisRoom.setRoomName(rs.getString("name"));

            return thisRoom;

        }

    }
}
