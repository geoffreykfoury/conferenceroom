/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.confroom.DAO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author gkfoury
 */
@Repository
public class User_Meeting_DAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String GET_USERS_INFO = "SELECT users.firstName, users.lastName\n"
            + "FROM conference.user_meeting\n"
            + "INNER JOIN userinfo\n"
            + "ON user_meeting.userID=users.userID where scheduleID= '14'";

    private final String INSERT_NEW_MEETING_AND_ID = "INSERT INTO user_meeting (scheduleID, userID) VALUES (?, (SELECT userID FROM userinfo WHERE email=?));";

    public void syncWithMeetingCreate(int scheduleID, String email) {

        jdbcTemplate.update(INSERT_NEW_MEETING_AND_ID, scheduleID, email);
    }

}
