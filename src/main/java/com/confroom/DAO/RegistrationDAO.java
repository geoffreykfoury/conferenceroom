package com.confroom.DAO;

import com.confroom.models.UserRegister;
import com.confroom.models.VerificationToken;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Geoffrey
 */
@Repository
public class RegistrationDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private final String INSERT_NEW_USER = "insert into users set username  = ?, password = ?, enabled = ?";
    private final String ASSIGN_ROLE = "insert into authorities set username = ?, authority = ?";
    private final String ADD_REG_TOKEN = "insert into token set id = ?, token = ?, expire = ?";
    private final String TOKEN_CHECK = "select id from token where token = ?";
    private final String DELETE_TOKEN = "delete from token where id=?";
    private final String DELETE_AUTHORITY = "delete from authorities where username=?";
    private final String GET_ALL_TOKENS = "select * from token";

    public String newUser(UserRegister newUser) {

        try {
            jdbcTemplate.update(INSERT_NEW_USER, newUser.getUsername(), newUser.getPassword(), newUser.getEnabled());

        } catch (DataAccessException e) {
            return "Initial update incomplete " + e.getMessage() + "----" + e.toString();
        }

        addAuthority(newUser.getUsername());

        return newUser.getUsername();
    }

    public void addAuthority(String username) {

        jdbcTemplate.update(ASSIGN_ROLE, username, "user");

    }

    public void addRegToken(VerificationToken newToken) {

        jdbcTemplate.update(ADD_REG_TOKEN, newToken.getUserID(), newToken.getToken(), newToken.getExpiration());
    }

    public String addRegToken(String token) {

        return (jdbcTemplate.queryForObject(TOKEN_CHECK, new Object[]{token}, String.class));

    }

    public void deleteToken(String username) {

        jdbcTemplate.update(DELETE_TOKEN, username);

    }

    public void deleteAuthority(String username) {

        jdbcTemplate.update(DELETE_AUTHORITY, username);

    }

    public List getAllTokens() {
        List<VerificationToken> tokens = jdbcTemplate.query(GET_ALL_TOKENS, new RegistrationDAO.TokenMapper());

        return tokens;

    }

    private class TokenMapper implements RowMapper<VerificationToken> {

        public TokenMapper() {
        }

        @Override
        public VerificationToken mapRow(ResultSet rs, int i) throws SQLException {

            VerificationToken newToken = new VerificationToken();

            newToken.setUserID(rs.getString("id"));
            newToken.setExpiration(rs.getTimestamp("expire"));

            return newToken;
        }
    }

}
